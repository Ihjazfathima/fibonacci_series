using System.Buffers.Text;
using System.Reflection.Metadata;
using System.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace fibonacci.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class fibonacciController : ControllerBase
    {
        [HttpPost]
        public fibcls post(fibonacci user)
        {
            string s = user.input;
            char[] str = s.ToCharArray();
            int value = 1, length = s.Length, j = 2;
            
            while (length>=value)
            {   
                    int ascii = (int)str[value - 1];
                    if(ascii==122)ascii =97;
                    else if(ascii == 90) ascii=65;
                    else if(ascii==102) ascii=112;
                    else if (ascii==70) ascii=80;
                    else if((ascii <65)||(ascii >=83 && ascii<=88) ||(ascii>=115 && ascii<=120)||(ascii >=91 && ascii <=96 )) ascii=0;
                    else ascii++;
                     if(ascii!=0)str[value - 1] = (char)ascii;
                    //str[value - 1] = (char)(ascii==122?65:(ascii==90?97:ascii+1));                  
                    value = fib(j);
                    j++;                    
            }
            string result = new string(str);
            fibcls res = new fibcls() { output = result };
            return res;
        }
        
        public int fib(int n)
        {
            int num1 = 0, num2 = 1, temp = 1;
            for (int i = 2; i <= n; i++)
            {
                temp = num1 + num2;

                num1 = num2;
                num2 = temp;
            }
            return temp;
        }
    }
}