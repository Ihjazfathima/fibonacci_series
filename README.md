# **Create Clone Project**

1. Create a clone from this url 'git clone https://gitlab.com/Ihjazfathima/fibonacci_series.git '
2. You will get a folder named fibbonacci_series.
3. Open fibonacci_series folder and select the path & enter the CMD comment in folder path then you will get comment prompt and Enter   code .
4. Automatically that application open in visual studio code.


# **Working Process**
1. Run the application (dotnet run) and select the application url http://localhost:5000/api/fibonacci
2. Open postman application and paste the application url http://localhost:5000/api/fibonacci
3. Select the method "POST" and body -> raw-> json
4. Enter the values like:
        {
            "input":"abcdefghijkl"
        }
          
5. You will get output like:
        {
            "output": "ccddffgiijkl"
        } 

    

